# edgolf

Inspired by https://www.vimgolf.com/ I made a little script to check solutions using ed(1) the standard unix text editor.

Create an original file such as boxit, then a set of ed instructions/commands in boxit.ed.

Run bash edgolf.sh boxit

Will report whether you reached the goal and how many characters were used.

boxit.goal is how the file should end up.

More examples should be added.
