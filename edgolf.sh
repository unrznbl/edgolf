echo "w !cat > %.result
Q
" | cat $1.ed - | ed -s $1
set -e
diff $1.result $1.goal
set +e
echo -n "score: "
echo "q
" | ed $1.ed
